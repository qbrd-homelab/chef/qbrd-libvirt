name              'qbrd-libvirt'
maintainer        'QubitRenegade'
maintainer_email  'qubitrenegade@protonmail.com'
license           'MIT'
description       'Installs/Configures qbrd-libvirt'
long_description  IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version           '0.1.0'
chef_version      '>= 13.0'

supports 'centos'

issues_url 'https://gitlab.com/qbrd-homelab/chef/qbrd-libvirt/issues'
source_url 'https://gitlab.com/qbrd-homelab/chef/qbrd-libvirt'

depends 'chef-client'
