#
# Cookbook:: qbrd-libvirt
# Recipe:: default
#
# The MIT License (MIT)
#
# Copyright:: 2019, QubitRenegade
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

package node['qbrd-libvirt']['packages'].select { |_k, v| v }.map { |k, _v| k } do
  action :install
  notifies :run, 'bash[setup-bridge]', :delayed
end

file '/root/bridge-setup.sh' do
  content <<~EO_BRIDGE
    #!/usr/bin/env bash
    set +x

    BR_NAME=#{node['qbrd-libvirt']['bridge']['name']}
    BR_INTERFACE=#{node['qbrd-libvirt']['bridge']['interface']}
    # Get our device UUID
    BR_INT_OG_UUID=$(nmcli -g GENERAL.CON-UUID device show "${BR_INTERFACE}")

    # Create bridge
    nmcli connection add type bridge autoconnect yes con-name "{BR_NAME}" ifname "${BR_NAME}"

    # Disable STP
    nmcli connection modify "${BR_NAME}" bridge.stp no

    # Add our interface to our bridge
    nmcli connection add type bridge-slave autoconnect yes con-name ${BR_INTERFACE} ifname ${BR_INTERFACE} master ${BR_NAME}

    # Turn off our old interface
    nmcli con down "${BR_INTERFACE}"

    # Turn on our new bridge
    nmcli con up "${BR_NAME}"

    # delete our old interface config
    nmcli con delete "${BR_INT_OG_UUID}"
  EO_BRIDGE
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

bash 'setup-bridge' do
  code '/root/bridge-setup.sh'
  action :nothing
end

service 'libvirtd' do
  action [:enable, :start]
end
