default['qbrd-libvirt'] = {
  'packages' => {
    'bridge-utils' => true,
    'libvirt' => true,
    'virt-install' => true,
    'qemu-kvm' => true,
    'virt-top' => true,
    'libguestfs-tools' => true,
    'virt-manager' => true,
  },
  'bridge' => {
    # make up a bridge name, I like br0
    'name' => 'br0',
    # this will vary depending on the mobo/eth card.  see `ip addr` to find device name
    'interface' => 'enp35s0',
  },
}
